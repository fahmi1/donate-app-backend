<h3>PROJECT DONATE APP BACKEND</h3>

<p>
    This is backend for Ez Donate App. You can download donate app for ios at https://gitlab.com/fahmi1/donate-app<br>
    <br>
    Backend Demo is run at http://donateapp.jobinic.com/
</p>

<h3>INTRODUCTION</h3>
<p>
This backend is basic API to support Ez Donate. A lot of improvement can be done. It only took me 1-2 days to developed<br>
this sample project. Hope can give your idea my coding dna and style. TQ
</p>

<p>
    <b>Specifications :</b><br>
    1. Codeigniter 3.0<br>
    2. HMVC Modular Extension<br>
    3. Authorize.net Library<br>
    4. PHP 5<br>
    5. Ubuntu 14.05 LS<br>
    6. Short tag in php.ini must be enable e.g support "<?" & "<?php"
</p>

<h3>INSTALLATION & SETTINGS</h3>
<p>
    1. Download Script<br>
    2. Place all script in in the url<br>
    3. Go to Application/Config/config.php and change your $config['base_url']<br>
    4. Go to Application/Config/database.php and change your database connection<br>
    5. Go to Database Management / Phpmyadmin. Import donatapp.sql. Pre Api key is installed in api table. you can check at api_key table<br>
    6. Go to Application/module/payment/config/authorize_net.php to setup your Authorize.net account <br>
    7. Go to Application/module/payment/config/bank_transfer.php to setup your bank info
</p>


<h3>USER API DOCUMENTATION</h3>

<p>
    
    URL : <b>http://[Your URL]/api/register/?api_key=[API KEY]</b><br> 
    Description : To register new user
    
    Request :<br>
    (Require)[POST]name<br>
    (Require)[POST]email<br>
    (Require)[POST]password<br>
    <br>
    Return : JSON<br>
    "status"=>[STATUS],
    "status_code"=>[STATUS CODE],
    "reason"=>[REASON DESCRIPTION],
    "function_name"=>[FUNCTION NAME],
    "response"=>[USER DATA]
</p>


<p>
    
    URL : <b>http://[Your URL]/api/login/?api_key=[API KEY]</b><br> 
    Description : To login user
    
    Request :<br>
    (Require)[POST]email<br>
    (Require)[POST]password<br>
    <br>
    Return : JSON<br>
    "status"=>[STATUS],
    "status_code"=>[STATUS CODE],
    "reason"=>[REASON DESCRIPTION],
    "function_name"=>[FUNCTION NAME],
    "response"=>[USER DATA]
</p>

<p>
    
    URL : <b>http://[Your URL]/api/login_fb/?api_key=[API KEY]</b><br> 
    Description : If user using FB login in Mobile app. Supply this info to communicate with server
    
    Request :<br>
    (Require)[POST]fb_id<br>
    (Require)[POST]fb_profile_url (image url)<br>
    (Require)[POST]fb_name<br>
    (Require)[POST]fb_email<br>
    <br>
    Return : JSON<br>
    "status"=>[STATUS],
    "status_code"=>[STATUS CODE],
    "reason"=>[REASON DESCRIPTION],
    "function_name"=>[FUNCTION NAME],
    "response"=>[USER DATA]
</p>

<p>
    
    URL : <b>http://[Your URL]/api/update_email/?api_key=[API KEY]</b><br> 
    Description : Update user email
    
    Request :<br>
    (Require)[POST]email<br>
    (Require)[POST]user_hash_id<br>
    <br>
    Return : JSON<br>
    "status"=>[STATUS],
    "status_code"=>[STATUS CODE],
    "reason"=>[REASON DESCRIPTION],
    "function_name"=>[FUNCTION NAME],
    "response"=>[NEW USER DATA]
</p>

<p>
    
    URL : <b>http://[Your URL]/api/update_name/?api_key=[API KEY]</b><br> 
    Description : Logout 
    
    Request :<br>
    (Require)[POST]name<br>
    (Require)[POST]user_hash_id<br>
    <br>
    Return : JSON<br>
    "status"=>[STATUS],
    "status_code"=>[STATUS CODE],
    "reason"=>[REASON DESCRIPTION],
    "function_name"=>[FUNCTION NAME],
    "response"=>[NEW USER DATA]
</p>

<p>
    
    URL : <b>http://[Your URL]/api/logout/?api_key=[API KEY]</b><br> 
    Description : Update user email
    
    Request :<br>
    (Require)[POST]user_hash_id<br>
    <br>
    Return : JSON<br>
    "status"=>[STATUS],
    "status_code"=>[STATUS CODE],
    "reason"=>[REASON DESCRIPTION],
    "function_name"=>[FUNCTION NAME],
    "response"=>nil
</p>

<p>
    
    URL : <b>http://[Your URL]/api/member/?api_key=[API KEY]</b><br> 
    Description : Get user data by hash_id
    
    Request :<br>
    (Require)[POST]user_hash_id<br>
    <br>
    Return : JSON<br>
    "status"=>[STATUS],
    "status_code"=>[STATUS CODE],
    "reason"=>[REASON DESCRIPTION],
    "function_name"=>[FUNCTION NAME],
    "response"=>[USER DATA]
</p>

<h3>CHECKOUT API DOCUMENTATION</h3>

<p>
    
    URL : <b>http://[Your URL]/api/pay_now/?api_key=[API KEY]</b><br> 
    Description : Pay now wrapping all infomation from create invoice to payment process
    
    Request :<br>
    (Require)[POST]billing_name<br>
    (Require)[POST]billing_email<br>
    (Require)[POST]user_hash_id<br>
    (Require)[POST]currency<br>
    (Require)[POST]total<br>
    (Require)[POST]payment_method_id<br>
    (Require)[POST]items_json<br>
        
        items_json : Array(<br>
                        Array(<br>
                                "item_id"=>[VALUE],<br>
                                "item_image_url"=>[VALUE],<br>
                                "item_name"=>[VALUE],<br>
                                "item_description"=>[VALUE],<br>
                                "item_country"=>[VALUE],<br>
                                "item_currency"=>[VALUE],<br>
                                "item_frequency"=>[VALUE],<br>
                                "item_value"=>[VALUE]<br>
                             )<br>
                           )
       
    (Require)[POST]payment_data_json : PLEASE CHECK PAYMENT METHOD API module/payment
    <br>
    Return : JSON<br>
    "status"=>[STATUS],
    "reason"=>[REASON DESCRIPTION],
    "invoice_no"=>[INVOICE NO],
    "data"=>[USER DATA]
</p>

<p>
    
    URL : <b>http://[Your URL]/api/pay_now_previous/?api_key=[API KEY]</b><br> 
    Description : Pay using previous success payment
    
    Request :<br>
    (Require)[POST]billing_name<br>
    (Require)[POST]billing_email<br>
    (Require)[POST]user_hash_id<br>
    (Require)[POST]currency<br>
    (Require)[POST]total<br>
    (Require)[POST]payment_method_id<br>
    (Require)[POST]items_json<br>
        
        items_json : Array(<br>
                        Array(<br>
                                "item_id"=>[VALUE],<br>
                                "item_image_url"=>[VALUE],<br>
                                "item_name"=>[VALUE],<br>
                                "item_description"=>[VALUE],<br>
                                "item_country"=>[VALUE],<br>
                                "item_currency"=>[VALUE],<br>
                                "item_frequency"=>[VALUE],<br>
                                "item_value"=>[VALUE]<br>
                             )<br>
                           )
       
   
    <br>
    Return : JSON<br>
    "status"=>[STATUS],
    "reason"=>[REASON DESCRIPTION],
    "invoice_no"=>[INVOICE NO],
    "data"=>[USER DATA]
</p>

