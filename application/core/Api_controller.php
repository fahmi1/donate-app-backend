<?php (defined('BASEPATH')) OR exit('No direct script access allowedx');

class Api_controller extends CI_Controller 
{
	
	
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('api_model');
		
		$this->check_api($_GET['api_key']);
		
	}	
	
	function check_api($api_key)
	{
		if($api_key == null)
		{
			echo json_encode(array("status"=>"failed","status_code"=>1,"reason"=>"Not specified Api Key"));
			die();
		}
		else
		{
			if($this->api_model->check_api_key($api_key) == true)
			{
				
				
			}
			else
			{
				echo json_encode(array("status"=>"failed","status_code"=>2,"reason"=>"Api key not found"));
				die();
			}
		}
		
	}
	
	
	
     
	
	
}
