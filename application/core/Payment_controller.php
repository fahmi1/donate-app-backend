<?php (defined('BASEPATH')) OR exit('No direct script access allowedx');

class Payment_controller extends CI_Controller 
{
	
	
	
	function __construct()
	{
		parent::__construct();
		
		$this->load->model(array('api/invoice_model'));
	
	}	
	
     /* 
		PRE CHECK INVOICE IN ANY PAYMENT METHOD
		return : JSON : DIE 
	*/
	
     function check_invoice($invoice_no)
     {
	     
	     if($this->invoice_model->check_invoice($invoice_no) == false)
	     {
		     echo json_encode(array("status"=>1,"reason"=>"invoice is expired or not available"));
		     die();
	     }
	     
     }
	
	
}
