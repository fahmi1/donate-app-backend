<?php
class Invoice_model extends CI_Model 
{
	
	function __construct()
	{
		parent::__construct();
	}
	
	
	function create_invoice($data,$user_hash_id)
	{
		
		$this->db->insert('invoices', $data);
		
		
		
		
	}
	
	function payment_success($invoice_no,$transaction_id,$approval_id,$payment_method_value)
	{
		$this->update_recour($invoice_no);
		$data['approval_id'] = $approval_id;
		$data['transaction_id'] = $transaction_id;
		$data['payment_method_value'] = $payment_method_value;
		$data['status'] = 2;
		$data['paid_date'] = date("Y-m-d H:i:s");;
		
		$this->db->where( array( 'invoice_no' => $invoice_no ) );
		return $this->db->update('invoices', $data);
	}
	
	
	function waiting_for_payment($invoice_no)
	{

		
		$data['status'] = 1;;
		
		$this->db->where( array( 'invoice_no' => $invoice_no ) );
		return $this->db->update('invoices', $data);
	}
	
	
	function update_recour($invoice_no)
	{

		
		$data['status'] = 1;;
		
		$this->db->where( array( 'invoice_id' => $invoice_no ) );
		return $this->db->update('donators_recurrence', $data);
	}
	
	/* 
		PRE CHECK INVOICE IN ANY PAYMENT METHOD
		return : BOOL 
	*/
	
	function check_invoice($invoice_number)
	{
	
		$this->db->where( array('invoice_no'=>$invoice_number) );
		$keys = $this->db->get('invoices')->result_array();
		
		foreach( $keys as  $row )
		{
			$invoice = $row;
		}
		
		if($keys)
		{
			if($invoice['status'] == 0)
			{
				if(date("U") < $invoice['expired'])
				{
					return true;
				}
				else
				{
					return false;
				}
				
			}
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}
		
		
	}
	
	
	function create_invoice_item($invoice_id,$item_id,$item_image_url,$item_name,$item_description,$item_country,$item_currency,$frequency,$value)
	{
		$data['invoice_id'] = $invoice_id;
		$data['item_id'] = $item_id;
		$data['item_image_url'] = $item_image_url;
		$data['item_name'] = $item_name;
		$data['item_description'] = $item_description;
		$data['item_country'] = $item_country;
		$data['item_currency'] = $item_currency;
		$data['frequency'] = $frequency;
		$data['value'] = $value;
		$this->db->insert('invoice_items', $data);
		
		
		
	}
	
	
	function get_invoiced( $invoice_no )
	{
		$this->db->where( array('invoice_no'=>$invoice_no) );
		return $this->db->get('invoices')->row_array( );
	}
	
	function get_invoiced_items( $invoice_no )
	{
		$this->db->where( array('invoice_id'=>$invoice_no) );
		return $this->db->get('invoice_items')->result_array();
	}
	
	
	
}
?>