<?php
class Api_model extends CI_Model 
{
	
	function __construct()
	{
		parent::__construct();
	}
	
	function check_api_key($api_key)
	{
	
		$this->db->where( array('key'=>$api_key,'status'=>'1') );
		$keys = $this->db->get('api_key')->result_array();
		
		
		
		
		if($keys)
		{
			return true;
		}
		else
		{
			return false;
		}
		
		
	}
	
	function sum_donation($user_hash_id)
	{
		
			$query = $this->db->query("SELECT SUM(total) as totals FROM invoices WHERE donator_hash_id='".$user_hash_id."'");
		
		
			$result = $query->result_array();
		
		foreach( $result as  $row )
		{
			$info = $row;
		}
		
		if($info['totals'] == null || $info['totals'] == 0)
		{
			return 0;
		}
		else
		{
			return $info['totals'];
		}
		
		
		
	}
	
	
	function check_email_db($email)
	{
	
		$this->db->where( array('email'=>$email) );
		$keys = $this->db->get('donators')->result_array();
		
		
		
		if($keys)
		{
			return true;
		}
		else
		{
			return false;
		}
		
		
	}
	
	function login_fb($user_id,$email)
	{
	
		$this->db->where( array('fb_id'=>$user_id,'email'=>$email) );
		$keys = $this->db->get('donators')->result_array();
		
		
		foreach( $keys as  $row )
		{
			$info = $row;
		}
		
		if($keys)
		{
			unset($info['password']);
			$this->update_last_request($info['hash_id']);
			return $this->user_by_hash_id($info['hash_id']);
		}
		else
		{
			return false;
		}
		
		
	}
	
	function login($email,$password)
	{
	
		$this->db->where( array('email'=>$email,'password'=>$password) );
		$keys = $this->db->get('donators')->result_array();
		
		
		foreach( $keys as  $row )
		{
			$info = $row;
		}
		
		if($keys)
		{
			unset($info['password']);
			$this->update_last_request($info['hash_id']);
			return $this->user_by_hash_id($info['hash_id']);
		}
		else
		{
			return false;
		}
		
		
	}
	
	function user_by_hash_id($user_hash_id)
	{
		$this->db->where( array('hash_id'=>$user_hash_id) );
		$keys = $this->db->get('donators')->result_array();
		
		
		foreach( $keys as  $row )
		{
			$info = $row;
		}
		
		if($keys)
		{
			unset($info['password']);
			
			if($info['profile_image'] == "")
			{
				$info['profile_image'] = base_url()."uploaded/noavatar.jpg";
			}
			else
			{
				$info['profile_image'] = base_url()."uploaded/".$info['profile_image'];
			}
			
					$total = $this->sum_donation($user_hash_id);
					$info['total'] = $total;
					$info['invoices'] = $this->get_invoiced_by_user($user_hash_id,2);
					$info['recurrence'] = $this->get_recourence_by_user($user_hash_id);
					
			return $info;
		}
		else
		{
			return false;
		}
	}
	
	function get_invoiced_by_user( $user_hash_id,$status )
	{
	
		$query = $this->db->query("SELECT * FROM invoices WHERE donator_hash_id='".$user_hash_id."' AND status='2' OR donator_hash_id='".$user_hash_id."' AND status='1' ORDER BY id DESC");
			
		return $query->result_array();
	}
	
	function get_recourence_by_user( $user_hash_id )
	{
	
		$query = $this->db->query("SELECT * FROM donators_recurrence WHERE user_hash_id='".$user_hash_id."' AND status='1' ORDER BY id DESC");
			
		return $query->result_array();
	}
	
	function edit_user($user_hash_id,$data)
	{
		$this->update_last_request($user_hash_id);
		$this->db->where( array( 'hash_id' => $user_hash_id ) );
		return $this->db->update('donators', $data);
	}
	
	
	function update_last_request($user_hash_id)
	{
		$data['last_request'] = date("Y-m-d H:i:s");
		$this->db->where( array( 'hash_id' => $user_hash_id ) );
		return $this->db->update('donators', $data);
	}
	
	function register_donator($data)
	{
		$data['last_request'] = date("Y-m-d H:i:s");
		$this->db->insert('donators', $data);
		
		return $this->user_by_hash_id($data['hash_id']);
	}
	
	
	function insert_donators_recurrence($data)
	{
		
		$this->db->insert('donators_recurrence', $data);
	}
	
	
}
?>