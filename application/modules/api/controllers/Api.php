<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api extends Api_controller {

	
	public function __construct()
	{
		parent::__construct();
		$this->load->model(array('api_model','invoice_model'));
		$this->load->helper(array('api_helper'));
		$this->load->library('encrdecrypt');
	}
	
	public function index()
	{
		$this->load->view('Api_view.php');
	}
	
	
	
	private function json_compile($status_code,$reason,$function_name,$data_array)
	{
		if($status_code > 0)
		{
			return json_encode(array("status"=>"failed","status_code"=>$status_code,"reason"=>$reason,"function_name"=>$function_name,"response"=>$data_array));
		}
		else
		{
			return json_encode(array("status"=>"success","status_code"=>$status_code,"reason"=>$reason,"function_name"=>$function_name,"response"=>$data_array));
		}
		
	}
	
	public function login_fb()
	{
		$fb_user_id = $this->input->post('fb_id');
		$fb_profile_url = $this->input->post('fb_profile_url');
		$fb_name = $this->input->post('fb_name');
		$fb_email = $this->input->post('fb_email');
		$array = array();
		$member = $this->api_model->login_fb($fb_user_id,$fb_email);
		if($member == false)
		{
				$content = file_get_contents("https://graph.facebook.com/".$fb_user_id."/picture?width=500&height=500&redirect=false");
				$datasss = json_decode($content, true);
				
				$name = date("U").".jpg";
				
				$url = $datasss["data"]["url"];
				//$name = basename($url);
				file_put_contents($_SERVER["DOCUMENT_ROOT"]."/uploaded/$name", file_get_contents($url));
				
				$fb_id = $this->input->post('fb_id');
				$registered_using = "facebook";
				$profile_image = $name;
			
			$data['name'] = strtoupper($fb_name);
			$data['email'] = $fb_email;
			$data['password'] = md5(rand(1, 999999));
			$data['created'] = date("Y-m-d H:i:s");
			$data['hash_id'] = md5(date("U"));
			$data['registered_using'] = $registered_using;
			$data['fb_id'] = $fb_id;
			$data['profile_image'] = $profile_image;
			$response = $this->api_model->register_donator($data);
			
			echo $this->json_compile(0,"Success","login",$response);
			
			
		}
		else
		{
			$data['is_login'] = 1;
			$this->api_model->edit_user($member['hash_id'],$data);
			echo $this->json_compile(0,"Success","login",$member);
			
		}
		
	}
	
	
	/* Function Start here */
	
	public function register()
	{
		
		$name = $this->input->post('name');
		$email = $this->input->post('email');
		$password = $this->input->post('password');
		
		$array = array();
		if($name == "")
		{	
			
			echo $this->json_compile(1,"Name is empty","register",$array);
		}
		else if($email == "")
		{
			echo $this->json_compile(2,"Email is empty","register",$array);
		}
		else if(check_email_address($email) == false)
		{
			echo $this->json_compile(3,"Email is not valid","register",$array);
		}
		else if($password == "")
		{
			echo $this->json_compile(4,"Password is empty","register",$array);
		}
		else
		{	
			if($this->api_model->check_email_db($email) == false)
			{
			$data['name'] = strtoupper($name);
			$data['email'] = $email;
			$data['password'] = md5($password);
			$data['created'] = date("Y-m-d H:i:s");
			$data['hash_id'] = md5(date("U"));
			$data['registered_using'] = "normal";
			$response = $this->api_model->register_donator($data);
			
			
			
			echo $this->json_compile(0,"Success","register",$response);
			}
			else
			{
				echo $this->json_compile(5,"Email existed","register",$array);
			}
			
		}
		
	}
	
	
	
	
	public function login()
	{
		
		$email = $this->input->post('email');
		$password = $this->input->post('password');
		$array = array();
		$member = $this->api_model->login($email,md5($password));
		if($member == false)
		{
				
			echo $this->json_compile(1,"Member not found","login",$array);
		}
		else
		{
			$data['is_login'] = 1;
			$this->api_model->edit_user($member['hash_id'],$data);
			
			echo $this->json_compile(0,"Success","login",$member);
			
		}
		
		
		
	}
	
	
	public function update_email()
	{
		
		$email = $this->input->post('email');
		$user_hash_id = $this->input->post('user_hash_id');
		
		$array = array();
		
		if($this->api_model->check_email_db($email) == false)
		{
			$data['email'] = $email;
			$this->api_model->edit_user($user_hash_id,$data);
			echo $this->json_compile(0,"Success","update_email",$this->api_model->user_by_hash_id($user_hash_id) );	
		}
		else
		{
			echo $this->json_compile(1,"Email Exist","update_email",$array);
		}
		
		
	}
	
	public function logout()
	{
		
		
		 
		/* START HERE */
		
		$user_hash_id = $this->input->post('user_hash_id');
		
		$array = array();
		
		$data['is_login'] = 0;
			$this->api_model->edit_user($user_hash_id,$data);
			echo $this->json_compile(0,"Success","logout",$array);
		
		
	}
	
	
	public function update_name()
	{
		
		
		 
		/* START HERE */
		$name = $this->input->post('name');
		$user_hash_id = $this->input->post('user_hash_id');
		
		$array = array();
		
		$data['name'] = $name;
			$this->api_model->edit_user($user_hash_id,$data);
			echo $this->json_compile(0,"Success","update_email",$this->api_model->user_by_hash_id($user_hash_id) );
		
		
	}
	
	
	
	public function member()
	{
		
		$user_hash_id = $this->input->post('user_hash_id');
		
		$array = array();
		$response = $this->api_model->user_by_hash_id($user_hash_id);
		
		if($response == false)
		{
			
			echo $this->json_compile(1,"ID not exist","member",$array);	
		}
		else
		{
			echo $this->json_compile(0,"Success","member",$response);
		}
		
		
	}
	
	public function pay_now_previous()
	{
		$billing_name = $this->input->post('billing_name');
		$billing_email = $this->input->post('billing_email');
		$user_hash_id = $this->input->post('user_hash_id');
		$currency = $this->input->post('currency');
		$total = $this->input->post('total');
		$payment_method_id = $this->input->post('payment_method_id');
		
		$items_json_url_encode = $this->input->post('items_json');
		$items_json = urldecode($items_json_url_encode);
		$items = json_decode($items_json);
		
		
		$data['billing_name'] = $billing_name;
		$data['billing_email'] = $billing_email;
		$data['donator_hash_id'] = $user_hash_id;
		$data['currency'] = $currency;
		$data['total'] = $total;
		$data['invoice_no'] = strtoupper(substr(uniqid(),7,12)); 
		$data['payment_method'] = $payment_method_id; 
		$data['created'] = date("Y-m-d H:i:s");
		$data['expired'] = date("U")+1800;
		$this->invoice_model->create_invoice($data,$user_hash_id);
		
		
		
		foreach($items as $key=>$item)
		{
			
			$this->invoice_model->create_invoice_item($data['invoice_no'],$item->{'item_id'},$item->{'item_image_url'},$item->{'item_name'},$item->{'item_description'},$item->{'item_country'},$item->{'item_currency'},$item->{'item_frequency'},$item->{'item_value'});
			
			if($item->{'item_frequency'} > 0)
			{
				$datas['item_id'] = $item->{'item_id'};
				$datas['item_image_url'] = $item->{'item_image_url'};
				$datas['item_name'] = $item->{'item_name'};
				$datas['item_description'] = $item->{'item_description'};
				$datas['item_country'] = $item->{'item_country'};
				$datas['item_currency'] = $item->{'item_currency'};
				$datas['frequency']=$item->{'item_frequency'};
				$datas['value'] = $item->{'item_value'};
				$datas['user_hash_id'] = $user_hash_id;
				$datas['created'] = date("Y-m-d H:i:s");
				$datas['invoice_id'] = $data['invoice_no'];
				$this->api_model->insert_donators_recurrence($datas);
			}
			
		}
		
		$response = $this->api_model->user_by_hash_id($user_hash_id);
		
		$payment = array(
			"card_number"=>$this->encrdecrypt->simple_decrypt($response['credit_card_number'],$user_hash_id),
			"expiry_date"=>$response['credit_card_expiry'],
			"cvv"=>$this->encrdecrypt->simple_decrypt($response['credit_card_cvv'],$user_hash_id),
			"name"=>$billing_name,
			"email"=>$billing_email,
			"total"=>$total
		);
		
		echo $this->curl_payment($payment_method_id,$data['invoice_no'],$user_hash_id,$payment);
		
		
		
		
	}
	
	
	public function pay_now()
	{
		$billing_name = $this->input->post('billing_name');
		$billing_email = $this->input->post('billing_email');
		$user_hash_id = $this->input->post('user_hash_id');
		$currency = $this->input->post('currency');
		$total = $this->input->post('total');
		$payment_method_id = $this->input->post('payment_method_id');
		
		$items_json_url_encode = $this->input->post('items_json');
		$items_json = urldecode($items_json_url_encode);
		$items = json_decode($items_json);
		
		
		$data['billing_name'] = $billing_name;
		$data['billing_email'] = $billing_email;
		$data['donator_hash_id'] = $user_hash_id;
		$data['currency'] = $currency;
		$data['total'] = $total;
		$data['invoice_no'] = strtoupper(substr(uniqid(),7,12)); 
		$data['payment_method'] = $payment_method_id; 
		$data['created'] = date("Y-m-d H:i:s");
		$data['expired'] = date("U")+1800;
		$this->invoice_model->create_invoice($data,$user_hash_id);
		
		
		
		foreach($items as $key=>$item)
		{
			
			$this->invoice_model->create_invoice_item($data['invoice_no'],$item->{'item_id'},$item->{'item_image_url'},$item->{'item_name'},$item->{'item_description'},$item->{'item_country'},$item->{'item_currency'},$item->{'item_frequency'},$item->{'item_value'});
			
			
			if($item->{'item_frequency'} > 0)
			{
				$datas['item_id'] = $item->{'item_id'};
				$datas['item_image_url'] = $item->{'item_image_url'};
				$datas['item_name'] = $item->{'item_name'};
				$datas['item_description'] = $item->{'item_description'};
				$datas['item_country'] = $item->{'item_country'};
				$datas['item_currency'] = $item->{'item_currency'};
				$datas['frequency']=$item->{'item_frequency'};
				$datas['value'] = $item->{'item_value'};
				$datas['user_hash_id'] = $user_hash_id;
				$datas['created'] = date("Y-m-d H:i:s");
				$datas['invoice_id'] = $data['invoice_no'];
				$this->api_model->insert_donators_recurrence($datas);
			}
			
		}
		
		$payment_json_url_encode = $this->input->post('payment_data_json');
		$payment_json = urldecode($payment_json_url_encode);
		$payment = json_decode($payment_json);
		
		
		echo $this->curl_payment($payment_method_id,$data['invoice_no'],$user_hash_id,$payment);
		
		
		
		
	}
	
	
	private function curl_payment($payment_id,$invoice_no,$user_hash_id,$data)
	{
		
		$ch = curl_init();

			curl_setopt($ch, CURLOPT_URL,"".base_url()."payment/".$payment_id."/api/");
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS,
			           "invoice_no=".$invoice_no."&user_hash_id=".$user_hash_id."&".urldecode(http_build_query($data))."");
			
			// in real life you should use something like:
			// curl_setopt($ch, CURLOPT_POSTFIELDS, 
			//          http_build_query(array('postvar1' => 'value1')));
			
			// receive server response ...
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			
			$server_output = curl_exec ($ch);
			
			curl_close ($ch);
			
			return $server_output;
	}
	
	
}
?>