<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/* Authorized.net Library */

class Invoiced extends CI_Controller {

	
	public function __construct()
	{
		parent::__construct();
		$this->load->model(array('api/invoice_model'));
		$this->load->config('payment/bank_transfer');
		
	}
	
	
	
	
	
	function no($invoiced_no)
	{
		$invoiced = $this->invoice_model->get_invoiced($invoiced_no);
		$phpdate = strtotime( $invoiced['paid_date'] );
		$invoice_date = date( 'd M Y', $phpdate );
		if($invoiced['status'] == 1)
		{
			$data['status'] = "PENDING";
		}
		else if($invoiced['status'] == 2)
		{
			$data['status'] = "PAID";
		}
		else 
		{
			$data['status'] = "DRAFT";
		}
		$data['bank_tansfer_config'] = array(
				"bank_name"=>$this->config->item('bank_name'),
				"swift_code"=>$this->config->item('swift_code'),
				"instruction"=>$this->config->item('instruction')
				);
		$data['transaction_id'] = strtoupper($invoiced['transaction_id']);
		$data['approval_id'] = strtoupper($invoiced['approval_id']);
		$data['billing_name'] = strtoupper($invoiced['billing_name']);
		$data['billing_email'] =$invoiced['billing_email'];
		$data['payment_method'] = strtoupper($invoiced['payment_method']);
		$data['payment_method_value'] = $invoiced['payment_method_value'];
		$data['invoice_no'] = strtoupper($invoiced_no);
		$data['paid_date'] = strtoupper($invoice_date);
		$data['currency'] = strtoupper($invoiced['currency']);
		$data['total'] = number_format($invoiced['total'],2);
		
		$data['items'] = $this->invoice_model->get_invoiced_items($invoiced_no);
		
		$this->load->view('Invoiced',$data);
		
		
	}
	
	
	
}
?>