
<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title>Invoiced - <? echo $invoice_no; ?></title>
    
    <style>
    .invoice-box{
	    max-width: 800px;
        margin:auto;
        font-size:16px;
        line-height:24px;
        font-family:'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
        color:#555;
    }
    
    .invoice-box table{
        width:100%;
        line-height:inherit;
        text-align:left;
    }
    
    .invoice-box table td{
        padding:5px;
        vertical-align:top;
    }
    
    .invoice-box table tr td:nth-child(2){
        text-align:right;
    }
    
    .invoice-box table tr.top table td{
        padding-bottom:20px;
    }
    
    .invoice-box table tr.top table td.title{
        font-size:45px;
        line-height:45px;
        color:#333;
    }
    
    .invoice-box table tr.information table td{
        padding-bottom:40px;
    }
    
    .invoice-box table tr.heading td{
        background:#eee;
        border-bottom:1px solid #ddd;
        font-weight:bold;
    }
    
    .invoice-box table tr.details td{
        padding-bottom:20px;
    }
    
    .invoice-box table tr.item td{
        border-bottom:1px solid #eee;
    }
    
    .invoice-box table tr.item.last td{
        border-bottom:none;
    }
    
    .invoice-box table tr.total td:nth-child(2){
        border-top:2px solid #eee;
        font-weight:bold;
    }
    /*
    @media only screen and (max-width: 600px) {
        .invoice-box table tr.top table td{
            width:100%;
            display:block;
            text-align:center;
        }
        
        .invoice-box table tr.information table td{
            width:100%;
            display:block;
            text-align:center;
        }
    }
    */
    </style>
</head>

<body>
    <div class="invoice-box">
        <table cellpadding="0" cellspacing="0">
            <tr class="top">
                <td colspan="2">
                    <table>
                        <tr>
                            <td class="title">
                                <img src="<? echo base_url()."/template/assets/logo-black.png" ?>" height="110"  >
                            </td>
                            
                            <td>
                                Invoice #: <b><? echo $invoice_no; ?></b><br>
                                Created: <b><? echo $paid_date; ?></b><br>
                                
                                <? if($payment_method == "CREDIT_CARD")
	                                { ?> 
	                                
	                                Transaction ID: <b><? echo $transaction_id; ?></b><br>  
	                                 Approval ID: <b><? echo $approval_id; ?></b><br> 
		                            <?  }  ?>
		                            
		                            Status: <b><? echo $status; ?></b><br>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            
            <tr class="information">
                <td colspan="2">
                    <table>
	                     <? if($payment_method == "BANK_TRANSFER"){ ?>
	                    <tr>
                            <td colspan="2">
                               
	                    BANK NAME :  <? echo $bank_tansfer_config['bank_name']; ?><br>
	                    SWIFT CODE :  <? echo $bank_tansfer_config['swift_code']; ?><br>
	                    INSTRUCTION :  <? echo $bank_tansfer_config['instruction']; ?><br>
	                    
                            </td>
                        </tr>
						<? } ?>
	                    
                        <tr>
                            <td>
                                <b>DONATE APP</b><br>
                                Birger Jarlsgatan 18<br>
                                114 34,Stockholm
                            </td>
                            
                            <td>
                                <b><? echo $billing_name; ?></b><br>
                                <? echo $billing_email; ?>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            
            <tr class="heading">
                <td>
                    Payment Method
                </td>
                
                <td>
                    
                </td>
            </tr>
            
            <tr class="details">
                <td>
                    <? echo $payment_method; ?>
                </td>
                
                <td>
                    <? echo $payment_method_value; ?>
                </td>
            </tr>
            
            <tr class="heading">
                <td>
                    Charities
                </td>
                
                <td>
                    Donations
                </td>
            </tr>
            <? foreach($items as $item){ ?>
            <tr class="item">
                <td>
                    <? echo $item['item_name']; ?>
                </td>
                
                <td>
                    <? echo $currency." ".$item['value']; ?>
                </td>
            </tr>
            
            <? } ?>
            
            <tr class="total">
                <td></td>
                
                <td>
                   Total: <? echo $currency; ?> <? echo $total; ?>
                </td>
            </tr>
        </table>
        
        <div style="margin-top:100px;" align="center">
	        <b>THANK YOU FOR YOUR DONATIONS</b> 
	        <br>
	        <br>
	        <img src="http://orig13.deviantart.net/bb06/f/2011/043/5/5/darth_vader_the_chibi_by_thistlexandra-d39esb6.gif" height="50" />
	        <br>
	        *** MAY FORCE BE WITH YOU ***

        </div>
        
    </div>
</body>
</html>
