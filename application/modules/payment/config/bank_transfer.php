<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// Authorize.net Account Info
$config['bank_name'] = '[BANK NAME HERE]';
$config['swift_code'] = '[SWIFT CODE HERE]'; 
$config['instruction'] = '[INSTRUCTION HOW TO PAY]'; 
/* EOF */