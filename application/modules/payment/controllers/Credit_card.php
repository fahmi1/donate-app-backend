<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/* Authorized.net Library */

class Credit_card extends Payment_controller {

	
	public function __construct()
	{
		parent::__construct();
		$this->load->model(array('api/invoice_model','api/api_model'));
		$this->load->library('authorize_net');
		$this->load->library('encrdecrypt');
		$this->load->helper(array('credit_card_helper'));
	}
	
	/* 
		public api to request and return JSON format
		Require : $_POST['invoice_no'] 
		return : JSON
	*/
	
	public function api()
	{
		
		
		$data = $this->request();
		echo json_encode($data);
		
	}
	
	/* 
		Send request to payment gateway
		return : array 
	*/
	
	private function request()
	{
		//will pre check invoice
		$this->check_invoice($this->input->post('invoice_no'));
		//
		
		/* Code Start here */
			
		$auth_net = array(
			'x_card_num'			=> ''.$this->input->post('card_number').'', // Visa
			'x_exp_date'			=> ''.$this->input->post('expiry_date').'',
			'x_card_code'			=> ''.$this->input->post('cvv').'',
			'x_description'			=> '#invoiced - '.$this->input->post('invoice_no').'',
			'x_amount'				=> ''.$this->input->post('total').'',
			'x_first_name'			=> ''.$this->input->post('name').'',
			'x_email'				=> ''.$this->input->post('email').'',
			'x_customer_ip'			=> $this->input->ip_address(),
			);
		$this->authorize_net->setData($auth_net);
		// Try to AUTH_CAPTURE
		if( $this->authorize_net->authorizeAndCapture() )
		{
			
			
			$this->invoice_model->payment_success($this->input->post('invoice_no'),$this->authorize_net->getTransactionId(),$this->authorize_net->getApprovalCode(),truncate_card($this->input->post('card_number')));
			
			//save credit card in user profile for future payment
			$data['credit_card_number'] = $this->encrdecrypt->simple_encrypt($this->input->post('card_number'),$this->input->post('user_hash_id'));
			$data['credit_card_expiry'] = str_replace(' ', '', $this->input->post('expiry_date'));
			$data['credit_card_cvv'] = $this->encrdecrypt->simple_encrypt($this->input->post('cvv'),$this->input->post('user_hash_id'));
			
			$data['credit_card_truncate'] = truncate_card($this->input->post('card_number'));
			
			$this->api_model->edit_user($this->input->post('user_hash_id'),$data);
			
			return array(
			"status"=>0,
			"reason"=>"successful",
			"data"=>array(
				"invoice_no"=>$this->input->post('invoice_no'),
				"transaction_id"=>$this->authorize_net->getTransactionId(),
				"approval_code"=>$this->authorize_net->getApprovalCode()
				)
				);
		}
		else
		{
			
			
			return array("status"=>1,"reason"=>$this->authorize_net->getError(),
			"data"=>array(
			"invoice_no"=>$this->input->post('invoice_no')
			));
		}
		
		/* Code END here */
	}
	
	
	//Instant Payment Notification
	
	public function ipn()
	{
		
		//Use this func
		//$this->invoice_model->payment_success($this->input->post('invoice_no'),$this->authorize_net->getTransactionId(),$this->authorize_net->getApprovalCode());
		
	}
	
	
	
	
	
	
}
?>