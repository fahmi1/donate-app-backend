<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/* Authorized.net Library */

class Bank_transfer extends Payment_controller {

	
	public function __construct()
	{
		parent::__construct();
		$this->load->model(array('api/invoice_model'));
		$this->load->config('bank_transfer');
	}
	
	/* 
		public api to request and return JSON format
		Require : $_POST['invoice_no'] 
		return : JSON
	*/
	
	public function api()
	{
		
		
		$data = $this->request();
		echo json_encode($data);
		
	}
	
	/* 
		Send request to payment gateway
		return : array 
	*/
	
	private function request()
	{
		//will pre check invoice
		$this->check_invoice($this->input->post('invoice_no'));
		//
		
		/* Code Start here */
			
		$this->invoice_model->waiting_for_payment($this->input->post('invoice_no'));
			
			
		return array(
			"status"=>0,
			"reason"=>"successful",
			"data"=>array(
				"invoice_no"=>$this->input->post('invoice_no'),
				"bank_name"=>$this->config->item('bank_name'),
				"swift_code"=>$this->config->item('swift_code'),
				"instruction"=>$this->config->item('instruction')
				)
				);
		
		/* Code END here */
	}
	
	//Instant Payment Notification
	
	public function ipn()
	{
		
		//Use this func
		//$this->invoice_model->payment_success($this->input->post('invoice_no'),$this->authorize_net->getTransactionId(),$this->authorize_net->getApprovalCode());
		
	}
	
	
	
}
?>