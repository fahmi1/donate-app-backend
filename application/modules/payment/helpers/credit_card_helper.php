<?
	


	function truncate_card($card_num) {
	    $padsize = (strlen($card_num) < 7 ? 0 : strlen($card_num) - 7);
		return substr($card_num, 0, 4) . str_repeat('X', $padsize). substr($card_num, -3);
	}
	
	function card_expiry_valid($month, $year) {
    $expiry_date = mktime(0, 0, 0, ($month + 1), 1, $year);
    return ($expiry_date > time());
	}

	function card_number_valid ($card_number) {
    $card_number = strrev(card_number_clean($card_number));
    $sum = 0;

    for ($i = 0; $i < strlen($card_number); $i++) {
      $digit = substr($card_number, $i, 1);

        // Double every second digit
        if ($i % 2 == 1) {
          $digit *= 2;
        }

        // Add digits of 2-digit numbers together
        if ($digit > 9)    {
          $digit = ($digit % 10) + floor($digit / 10);
        }

        $sum += $digit;
    }

    // If the total has no remainder it's OK
    return ($sum % 10 == 0);
	}
?>