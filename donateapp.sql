-- phpMyAdmin SQL Dump
-- version 3.5.8.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jan 05, 2016 at 02:25 PM
-- Server version: 5.5.34-0ubuntu0.12.04.1
-- PHP Version: 5.3.10-1ubuntu3.21

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `zadmin_donateapp`
--

-- --------------------------------------------------------

--
-- Table structure for table `api_key`
--

CREATE TABLE IF NOT EXISTS `api_key` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key` text NOT NULL,
  `description` text NOT NULL,
  `created` datetime NOT NULL,
  `by` bigint(20) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `api_key`
--

INSERT INTO `api_key` (`id`, `key`, `description`, `created`, `by`, `status`) VALUES
(1, '03e236b404ae7518d4feab8a2be4dc18', 'For ios application', '2016-01-02 00:00:00', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `donators`
--

CREATE TABLE IF NOT EXISTS `donators` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `profile_image` text NOT NULL,
  `name` text NOT NULL,
  `email` text NOT NULL,
  `password` text NOT NULL,
  `created` datetime NOT NULL,
  `hash_id` text NOT NULL,
  `is_login` int(11) NOT NULL DEFAULT '1',
  `last_request` datetime NOT NULL,
  `registered_using` text NOT NULL,
  `fb_id` bigint(20) NOT NULL,
  `credit_card_number` text NOT NULL,
  `credit_card_truncate` text NOT NULL,
  `credit_card_expiry` text NOT NULL,
  `credit_card_cvv` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `donators_recurrence`
--

CREATE TABLE IF NOT EXISTS `donators_recurrence` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `item_id` text NOT NULL,
  `item_image_url` text NOT NULL,
  `item_name` text NOT NULL,
  `item_description` text NOT NULL,
  `item_country` text NOT NULL,
  `item_currency` text NOT NULL,
  `frequency` int(11) NOT NULL,
  `value` float NOT NULL,
  `user_hash_id` text NOT NULL,
  `created` datetime NOT NULL,
  `invoice_id` text NOT NULL,
  `status` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `invoices`
--

CREATE TABLE IF NOT EXISTS `invoices` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `billing_name` text NOT NULL,
  `billing_email` text NOT NULL,
  `donator_hash_id` text NOT NULL,
  `currency` text NOT NULL,
  `total` float NOT NULL,
  `invoice_no` text NOT NULL,
  `payment_method` text NOT NULL,
  `payment_method_value` text NOT NULL,
  `created` datetime NOT NULL,
  `status` int(1) NOT NULL DEFAULT '0',
  `paid_date` datetime NOT NULL,
  `transaction_id` text NOT NULL,
  `approval_id` text NOT NULL,
  `expired` bigint(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `invoice_items`
--

CREATE TABLE IF NOT EXISTS `invoice_items` (
  `no` bigint(20) NOT NULL AUTO_INCREMENT,
  `invoice_id` text NOT NULL,
  `item_id` text NOT NULL,
  `item_image_url` text NOT NULL,
  `item_name` text NOT NULL,
  `item_description` text NOT NULL,
  `item_country` text NOT NULL,
  `item_currency` text NOT NULL,
  `frequency` int(11) NOT NULL,
  `value` float NOT NULL,
  PRIMARY KEY (`no`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
